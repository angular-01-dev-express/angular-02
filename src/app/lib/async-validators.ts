import { AbstractControl } from '@angular/forms';

interface CustomValidationMessage {
  type: string;
  description: string;
  stackTrace?: string;
  value?: any;
}

export const BoliviaCityValidator = (control: AbstractControl): Promise<CustomValidationMessage | null> => {
  const validCities = ['Cochabamba', 'La Paz', 'Santa Cruz'].map(city => city.toLowerCase());
  const isValid = !control.value || validCities.findIndex(city => city === control.value.toLowerCase()) >= 0;
  const message: CustomValidationMessage = {
    type: 'customValidation',
    description: `The city ${control.value} is not a valid City`
  };

  // return Promise.resolve(null);

  // return Promise.resolve(isValid ? null : message);

  return new Promise((resolve) => {
    setTimeout(() => resolve(isValid ? null : message), 1000);
  });
};
