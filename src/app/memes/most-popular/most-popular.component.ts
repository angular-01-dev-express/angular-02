import { Component, OnInit } from '@angular/core';

import { MemegeneratorService } from '../memegenerator.service';
import { Meme } from '../meme';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { MemeSelectedService } from '../meme-selected.service';

@Component({
  selector: 'app-most-popular',
  templateUrl: './most-popular.component.html',
  styleUrls: ['./most-popular.component.scss']
})
export class MostPopularComponent implements OnInit {
  memes: Array<Meme>;
  isLoading: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private memegenerator: MemegeneratorService,
    private memeSelected: MemeSelectedService
  ) { }

  ngOnInit() {
    this.isLoading = true;

    this.memegenerator.topTen().subscribe(response => {
      this.memes = response['result'];
      this.isLoading = false;
    }, (err) => {
      console.error(err);
      this.isLoading = false;
    });
  }

  goToDetail(meme: Meme) {
    // Do not use - never
    // window.location.href = `http://localhost:4200/memes/most-popular/${id}`;

    // this.router.navigate([`./${id}`], { relativeTo: this.route });

    this.memeSelected.selectedMeme.next(meme);
  }
}
