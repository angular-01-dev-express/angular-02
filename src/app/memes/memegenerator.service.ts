import { Injectable } from '@angular/core';
import { Meme } from './meme';
import { HttpClient } from '@angular/common/http';

import { URL, API_KEY as apiKey } from './memes.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MemegeneratorService {
  constructor(private http: HttpClient) {}

  topTen(): Observable<any> {
    const options = {
      params: { apiKey }
    };

    return this.http.get(`${URL}/Generators_Search`, options);
  }
}
