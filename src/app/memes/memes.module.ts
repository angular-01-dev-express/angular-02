import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MostPopularComponent } from './most-popular/most-popular.component';
import { MemesRoutingModule } from './memes-routing.module';
import { MemeDetailsComponent } from './meme-details/meme-details.component';
import { SharedModule } from '../shared/shared.module';
import { MemeSelectedComponent } from './meme-selected/meme-selected.component';

@NgModule({
  imports: [
    CommonModule,
    MemesRoutingModule,
    SharedModule
  ],
  declarations: [
    MostPopularComponent,
    MemeDetailsComponent,
    MemeSelectedComponent
  ]
})
export class MemesModule { }
