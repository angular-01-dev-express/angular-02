import { TestBed, inject } from '@angular/core/testing';

import { MemegeneratorService } from './memegenerator.service';

describe('MemegeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemegeneratorService]
    });
  });

  it('should be created', inject([MemegeneratorService], (service: MemegeneratorService) => {
    expect(service).toBeTruthy();
  }));
});
