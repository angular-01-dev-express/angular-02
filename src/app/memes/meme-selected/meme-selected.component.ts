import { Component, OnInit } from '@angular/core';
import { MemeSelectedService } from '../meme-selected.service';
import { Meme } from '../meme';

@Component({
  selector: 'app-meme-selected',
  templateUrl: './meme-selected.component.html',
  styleUrls: ['./meme-selected.component.scss']
})
export class MemeSelectedComponent implements OnInit {
  currentMeme: Meme;

  constructor(private memeSelected: MemeSelectedService) {
    // Defined
    // memeSelected.selectedMeme.subscribe((newMeme: Meme) => this.handleSubscription(newMeme));

    // Anonymous
    memeSelected.selectedMeme.subscribe((newMeme: Meme) => {
      this.currentMeme = newMeme;
    });
  }

  ngOnInit() {
  }

  handleSubscription(newMeme: Meme) {
    this.currentMeme = newMeme;
  }

  removeCurrent() {
    this.memeSelected.selectedMeme.next(null);
  }
}
