import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemeSelectedComponent } from './meme-selected.component';

describe('MemeSelectedComponent', () => {
  let component: MemeSelectedComponent;
  let fixture: ComponentFixture<MemeSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemeSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemeSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
