import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-meme-details',
  templateUrl: './meme-details.component.html',
  styleUrls: ['./meme-details.component.scss']
})
export class MemeDetailsComponent implements OnInit {
  id: String;

  constructor(route: ActivatedRoute) {
    route.params.subscribe(({ id }) => {
      this.id = id;
    });
  }

  ngOnInit() {
  }

}
