import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MostPopularComponent } from './most-popular/most-popular.component';
import { MemeDetailsComponent } from './meme-details/meme-details.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'most-popular',
        component: MostPopularComponent
      },
      {
        path: 'most-popular/:id',
        component: MemeDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class MemesRoutingModule { }
