import { TestBed, inject } from '@angular/core/testing';

import { MemeSelectedService } from './meme-selected.service';

describe('MemeSelectedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemeSelectedService]
    });
  });

  it('should be created', inject([MemeSelectedService], (service: MemeSelectedService) => {
    expect(service).toBeTruthy();
  }));
});
