import { Injectable } from '@angular/core';
import { Meme } from './meme';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MemeSelectedService {
  selectedMeme = new Subject<Meme>();

  constructor() { }
}
