import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BoliviaCityValidator } from 'src/app/lib/async-validators';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  group: FormGroup;
  person: any;

  constructor(private fb: FormBuilder) {
    // this.group = new FormGroup({
    //   name: new FormControl('', [Validators.required]),
    //   lastName: new FormControl('', [Validators.required, Validators.minLength(5)]),
    //   motherLastName: new FormControl()
    // });
    this.group = this.fb.group({
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required, Validators.minLength(5)]],
      motherLastName: [''],

      address: this.fb.group({
        city: ['', [], [BoliviaCityValidator]]
      })
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.person = this.group.getRawValue();
  }
}
