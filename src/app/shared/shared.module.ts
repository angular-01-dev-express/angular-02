import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { SpinnerComponent } from './spinner/spinner.component';
import { ValidatorComponent } from './validator/validator.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SpinnerComponent,
    ValidatorComponent
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    SpinnerComponent,
    ValidatorComponent
  ]
})
export class SharedModule { }
