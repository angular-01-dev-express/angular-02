import { Component } from '@angular/core';

@Component({
  selector: 'app-desktop-canvas',
  templateUrl: './desktop-canvas.component.html',
  styleUrls: ['./desktop-canvas.component.scss']
})
export class DesktopCanvasComponent {
  desktops: any[];
  face = 'front';

  constructor() {
    this.desktops = [{
      name: 'Desktop 0',
      face: 'front'
    }, {
      name: 'Desktop 1',
      face: 'back'
    }, {
      name: 'Desktop 2',
      face: 'right'
    }, {
      name: 'Desktop 3',
      face: 'left'
    }, {
      name: 'Desktop 4',
      face: 'top'
    }, {
      name: 'Desktop 5',
      face: 'bottom'
    }];
  }

  changeFace(face: string) {
    this.face = face;
  }
}
