import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MockComponent } from 'mock-component';

import { DesktopCanvasComponent } from './desktop-canvas.component';
import { DesktopFaceComponent } from '../desktop-face/desktop-face.component';

describe('DesktopCanvasComponent', () => {
  let component: DesktopCanvasComponent;
  let fixture: ComponentFixture<DesktopCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockComponent(DesktopFaceComponent),
        DesktopCanvasComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have six faces', () => {
    const faces = component.desktops.length;
    expect(faces).toBe(6);
  });

  it('should change their face', () => {
    component.changeFace('right');
    expect(component.face).toBe('right');
  });
});
