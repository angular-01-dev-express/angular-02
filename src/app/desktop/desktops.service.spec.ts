import { TestBed, inject } from '@angular/core/testing';

import { DesktopsService } from './desktops.service';
import { Icon } from './objects/icon';

describe('DesktopsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DesktopsService]
    });
  });

  it('should be created', inject([DesktopsService], (service: DesktopsService) => {
    expect(service).toBeTruthy();
  }));

  it('should have defined 6 desktops', inject([DesktopsService], (service: DesktopsService) => {
    expect(service.desktops.length).toBe(6);
  }));

  it('should move an icon to a desktop', inject([DesktopsService], (service: DesktopsService) => {
    const desktopSource = 0;
    const icon: Icon = {
      name: 'Icon test',
      ext: '.ts'
    };

    expect(service.desktops[desktopSource].icons.length).toBe(3);
    service.move(desktopSource, icon);
    expect(service.desktops[desktopSource].icons.length).toBe(4);

  }));

  it('should remove an existing desktop', inject([DesktopsService], (service: DesktopsService) => {
    const desktopSource = 0;
    const indexIcon = 0;

    expect(service.desktops[desktopSource].icons.length).toBe(3);
    service.remove(desktopSource, indexIcon);
    expect(service.desktops[desktopSource].icons.length).toBe(2);
  }));
});
