export interface Icon {
  name: string;
  ext: string;
}
