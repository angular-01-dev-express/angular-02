import { Icon } from './icon';

export interface Desktop {
  icons: Icon[];
}
