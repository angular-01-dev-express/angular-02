import { Component, OnInit, Input } from '@angular/core';
import { DesktopsService } from '../desktops.service';
import { Icon } from '../objects/icon';
import { Desktop } from '../objects/desktop';
import { Store, select } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-desktop-face',
  templateUrl: './desktop-face.component.html',
  styleUrls: ['./desktop-face.component.scss']
})
export class DesktopFaceComponent implements OnInit {
  icons: Icon[];
  icons$: Observable<Icon[]>;
  sub: Subscription;

  @Input('face') face: number;
  @Input('position') position: string;

  constructor(private store: Store<any>,
    private desktopsService: DesktopsService) { }

  ngOnInit() {
    const selector = select((state: any) => {
      return state.desktop[this.face].icons;
    });
    this.icons$ = this.store.pipe(selector);

    // this.sub = this.store.pipe(select(state => {
    //   return state.desktop[this.face].icons;
    // })).subscribe(icons => {
    //   this.icons = icons;
    // });

    // const { icons = [] } = this.desktopsService.desktops[this.face];
    // this.icons = icons;

    // Todo
    // this.desktopsService.desktops.subscribe((desktop: Desktop) => {
    //   this.icons = desktop.icons;
    // });
  }

  moveTo(desktop: number, icon: number) {
    // const iconToMove = this.desktopsService.remove(this.face, icon);
    // this.desktopsService.move(desktop, iconToMove);
    this.store.dispatch({
      type: 'MOVE_ICON',
      payload: {
        origin: this.face,
        source: desktop,
        index: icon
      }
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    // this.sub.unsubscribe();
  }
}
