import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopFaceComponent } from './desktop-face.component';

describe('DesktopFaceComponent', () => {
  let component: DesktopFaceComponent;
  let fixture: ComponentFixture<DesktopFaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesktopFaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopFaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
