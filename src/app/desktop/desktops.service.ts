import { Injectable } from '@angular/core';
import { Desktop } from './objects/desktop';
import { Icon } from './objects/icon';

@Injectable({
  providedIn: 'root'
})
export class DesktopsService {
  desktops: Desktop[];
  // Todo
  // desktops: Subject[]

  constructor() {
    this.desktops = <Desktop[]>[{
      // Desktop 0
      icons: [{
        name: 'icon 1'
      }, {
        name: 'icon 2'
      }, {
        name: 'icon 3'
      }]
    }, {
      // Desktop 1
      icons: [{
        name: 'icon 1'
      }]
    }, {
      // Desktop 2
      icons: [{
        name: 'icon 1'
      }, {
        name: 'icon 2'
      }, {
        name: 'icon 3'
      }]
    }, {
      // Desktop 3
      icons: [{
        name: 'icon 1'
      }, {
        name: 'icon 2'
      }, {
        name: 'icon 3'
      }]
    }, {
      // Desktop 4
      icons: [{
        name: 'icon 1'
      }, {
        name: 'icon 2'
      }, {
        name: 'icon 3'
      }]
    },  {
      // Desktop 5
      icons: [{
        name: 'icon 1'
      }, {
        name: 'icon 2'
      }, {
        name: 'icon 3'
      }]
    }];
  }

  remove(desktopOrigin: number, index: number): Icon {
    const [icon] = this.desktops[desktopOrigin].icons.splice(index, 1);

    return icon;
  }

  move(desktopSource: number, icon: Icon) {
    this.desktops[desktopSource].icons.push(icon);
  }
}
