import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DesktopCanvasComponent } from './desktop-canvas/desktop-canvas.component';

const routes: Routes = [{
  path: '',
  component: DesktopCanvasComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesktopRoutingModule { }
