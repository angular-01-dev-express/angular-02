import { Action } from '@ngrx/store';

const initialState = [{
  // Desktop 0
  icons: [{
    name: 'icon 1'
  }, {
    name: 'icon 2'
  }, {
    name: 'icon 3'
  }]
}, {
  // Desktop 1
  icons: [{
    name: 'icon 1'
  }]
}, {
  // Desktop 2
  icons: [{
    name: 'icon 1'
  }, {
    name: 'icon 2'
  }, {
    name: 'icon 3'
  }]
}, {
  // Desktop 3
  icons: [{
    name: 'icon 1'
  }, {
    name: 'icon 2'
  }, {
    name: 'icon 3'
  }]
}, {
  // Desktop 4
  icons: [{
    name: 'icon 1'
  }, {
    name: 'icon 2'
  }, {
    name: 'icon 3'
  }]
},  {
  // Desktop 5
  icons: [{
    name: 'icon 1'
  }, {
    name: 'icon 2'
  }, {
    name: 'icon 3'
  }]
}];

export function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case 'MOVE_ICON':
      const { origin, source, index } = action['payload'];
      const [icon] = state[origin].icons.splice(index, 1);
      state[source].icons.push(icon);
      return state;
    default:
      return state;
  }
}
