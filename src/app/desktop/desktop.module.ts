import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { reducer } from './state/desktop.reducer';

import { DesktopRoutingModule } from './desktop-routing.module';
import { DesktopCanvasComponent } from './desktop-canvas/desktop-canvas.component';
import { DesktopFaceComponent } from './desktop-face/desktop-face.component';

@NgModule({
  imports: [
    CommonModule,
    DesktopRoutingModule,
    StoreModule.forFeature('desktop', reducer)
  ],
  declarations: [
    DesktopCanvasComponent,
    DesktopFaceComponent
  ]
})
export class DesktopModule { }
