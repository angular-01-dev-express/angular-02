import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
  {
    path: 'index',
    component: IndexComponent
  },
  {
    path: 'memes',
    loadChildren: './memes/memes.module#MemesModule'
  },
  {
    path: 'desktop',
    loadChildren: './desktop/desktop.module#DesktopModule'
  },
  {
    path: 'todo',
    loadChildren: './todo-list/todo-list.module#TodoListModule'
  },
  {
    path: 'products',
    loadChildren: './shopping/shopping.module#ShoppingModule'
  },
  {
    path: 'form',
    loadChildren: './forms/forms.module#FormsModule'
  },
  {
    path: '**',
    redirectTo: 'index'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
