import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TodoListRawComponent } from './todo-list-raw/todo-list-raw.component';

const routes: Routes = [{
  path: '',
  component: TodoListRawComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoListRoutingModule { }
