import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-add-new-task',
  templateUrl: './add-new-task.component.html',
  styleUrls: ['./add-new-task.component.scss']
})
export class AddNewTaskComponent implements OnInit {
  newTask: string;
  constructor(private store: Store<any>) {
    this.newTask = '';
  }

  ngOnInit() {
  }

  addTask() {
    this.store.dispatch({
      type: 'ADD_TASK',
      payload: {
        isComplete: false,
        description: this.newTask
      }
    });

    this.newTask = '';
  }

}
