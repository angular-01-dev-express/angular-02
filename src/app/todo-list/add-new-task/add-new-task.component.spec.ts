import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewTaskComponent } from './add-new-task.component';
import { FormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { TestStore } from 'src/app/imports/store.test';

describe('AddNewTaskComponent', () => {
  let component: AddNewTaskComponent;
  let fixture: ComponentFixture<AddNewTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      providers: [{
        provide: Store, useClass: TestStore
      }],
      declarations: [ AddNewTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
