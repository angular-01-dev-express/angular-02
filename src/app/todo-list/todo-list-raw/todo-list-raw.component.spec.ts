import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListRawComponent } from './todo-list-raw.component';
import { AddNewTaskComponent } from '../add-new-task/add-new-task.component';
import { FormsModule } from '@angular/forms';
import { TestStore } from 'src/app/imports/store.test';
import { Store } from '@ngrx/store';
import { HttpClient } from 'selenium-webdriver/http';
import { HttpClientModule } from '@angular/common/http';

describe('TodoListRawComponent', () => {
  let component: TodoListRawComponent;
  let fixture: ComponentFixture<TodoListRawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule
      ],
      providers: [{
        provide: Store, useClass: TestStore
      }, {
        provide: HttpClient, useClass: {}
      }],
      declarations: [ AddNewTaskComponent, TodoListRawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoListRawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
