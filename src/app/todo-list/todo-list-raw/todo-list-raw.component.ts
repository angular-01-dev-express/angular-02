import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-todo-list-raw',
  templateUrl: './todo-list-raw.component.html',
  styleUrls: ['./todo-list-raw.component.scss']
})
export class TodoListRawComponent implements OnInit {
  tasks$: Observable<any[]>;

  constructor(private store: Store<any>, private http: HttpClient) { }

  ngOnInit() {
    const selector = select('todo');
    this.tasks$ = this.store.pipe(selector);
  }

  checkTask(index) {
    this.store.dispatch({
      type: 'CHECK_TASK',
      payload: index
    });
  }
}
