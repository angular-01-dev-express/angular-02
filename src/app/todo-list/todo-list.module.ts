import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoListRoutingModule } from './todo-list-routing.module';
import { TodoListRawComponent } from './todo-list-raw/todo-list-raw.component';
import { StoreModule } from '@ngrx/store';
import { AddNewTaskComponent } from './add-new-task/add-new-task.component';
import { FormsModule } from '@angular/forms';
import { reducer } from './state/todo.reducer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TodoListRoutingModule,
    StoreModule.forFeature('todo', reducer)
  ],
  declarations: [TodoListRawComponent, AddNewTaskComponent]
})
export class TodoListModule { }
