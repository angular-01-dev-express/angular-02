const initialState = [{
  isComplete: false,
  description: 'Lorem Ipsum sit dolor'
}, {
  isComplete: false,
  description: 'Lorem Ipsum sit dolor'
}, {
  isComplete: false,
  description: 'Lorem Ipsum sit dolor'
}, {
  isComplete: false,
  description: 'Lorem Ipsum sit dolor'
}];

export function reducer(state = initialState, action) {
  switch (action.type) {
    case 'ADD_TASK':
      state.push(action.payload);
      return state;
    case 'CHECK_TASK':
      state[action.payload].isComplete = !state[action.payload].isComplete;
      return state;
    default:
      return state;
  }
}
