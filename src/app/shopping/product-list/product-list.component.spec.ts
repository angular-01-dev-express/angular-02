import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerComponent } from '../../shared/spinner/spinner.component';
import { ProductListComponent } from './product-list.component';
import { MockComponent } from 'mock-component';
import { Store } from '@ngrx/store';
import { TestStore } from 'src/app/imports/store.test';

describe('ProductListComponent', () => {
  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductListComponent,
        MockComponent(SpinnerComponent)
      ],
      providers: [{
        provide: Store, useClass: TestStore
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
