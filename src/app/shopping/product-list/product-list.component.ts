import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IProduct } from '../data/iproduct';
import { Store, select } from '@ngrx/store';

import * as fromShopping from '../state/shopping.state';
import * as ShoppingActions from '../state/shopping.actions';
import { allProducts, isLoading } from '../state/shopping.selectors';
import { AddProduct } from '../state/shopping.actions';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products$: Observable<IProduct[]>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<fromShopping.State>) { }

  ngOnInit() {
    this.products$ = this.store.pipe(select(allProducts));
    this.isLoading$ = this.store.pipe(select(isLoading));

    this.loadProducts();
  }

  loadProducts() {
    const action = new ShoppingActions.LoadProductsStore();
    this.store.dispatch(action);
  }

  addToBasket(product: IProduct) {
    const addProductAction = new AddProduct(product);
    this.store.dispatch(addProductAction);
  }
}
