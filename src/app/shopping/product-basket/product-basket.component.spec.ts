import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductBasketComponent } from './product-basket.component';
import { Store } from '@ngrx/store';
import { TestStore } from 'src/app/imports/store.test';

describe('ProductBasketComponent', () => {
  let component: ProductBasketComponent;
  let fixture: ComponentFixture<ProductBasketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductBasketComponent ],
      providers: [{
        provide: Store, useClass: TestStore
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductBasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
