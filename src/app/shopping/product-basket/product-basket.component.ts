import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IProduct } from '../data/iproduct';
import { Store, select } from '@ngrx/store';

import * as fromShooping from '../state/shopping.state';
import { basket, total } from '../state/shopping.selectors';

@Component({
  selector: 'app-product-basket',
  templateUrl: './product-basket.component.html',
  styleUrls: ['./product-basket.component.scss']
})
export class ProductBasketComponent implements OnInit {
  basket$: Observable<IProduct[]>;
  total$: Observable<number>;

  constructor(private store: Store<fromShooping.State>) { }

  ngOnInit() {
    this.basket$ = this.store.pipe(select(basket));
    this.total$ = this.store.pipe(select(total));
  }

}
