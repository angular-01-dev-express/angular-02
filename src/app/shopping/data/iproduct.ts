export class IProduct {
  name: string;
  description: string;
  price: number;
  image?: string;
  qty = 0;
}
