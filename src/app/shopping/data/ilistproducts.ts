import { IProduct } from './iproduct';

export interface IListProduct {
  isLoading: boolean;
  products: IProduct[];
  basket: IProduct[];
  total: number;
}
