import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingRoutingModule } from './shopping-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductBasketComponent } from './product-basket/product-basket.component';
import { IndexComponent } from './index/index.component';
import { StoreModule } from '@ngrx/store';

import { reducer } from './state/shopping.reducer';
import { SharedModule } from '../shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { ShoppingEffects } from './state/shopping.effects';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ShoppingRoutingModule,
    StoreModule.forFeature('shopping', reducer),
    EffectsModule.forRoot([ShoppingEffects])
  ],
  declarations: [
    ProductListComponent,
    ProductBasketComponent,
    IndexComponent
  ]
})
export class ShoppingModule { }
