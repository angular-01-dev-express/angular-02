import { Action } from '@ngrx/store';
import { IProduct } from '../data/iproduct';
import { ShoppingTypes } from './shopping.types';

export class AddProduct implements Action {
  readonly type = ShoppingTypes.AddProduct;
  constructor(public payload: IProduct) { }
}

export class LoadProducts implements Action {
  readonly type = ShoppingTypes.LoadProducts;
  constructor(public payload: IProduct[]) {}
}

export class SetLoadingStatus implements Action {
  readonly type = ShoppingTypes.SetLoadingStatus;
  constructor(public payload: boolean) {}
}

export class LoadProductsStore implements Action {
  readonly type = ShoppingTypes.LoadProductsStore;
}

export type ShoopingActions = AddProduct | LoadProducts | SetLoadingStatus;
