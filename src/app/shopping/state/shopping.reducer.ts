import { initialState, ShoppingState } from './shopping.state';
import { ShoopingActions } from './shopping.actions';
import { ShoppingTypes } from './shopping.types';

export function reducer(state = initialState, action: ShoopingActions): ShoppingState {
  switch (action.type) {
    case ShoppingTypes.AddProduct:
      const {basket} = state;
      const i = basket.findIndex( p => p.name === action.payload.name);
      if (i === -1) {
        const product = {...action.payload, qty: 1};
        basket.push(product);
      } else {
        basket[i].qty += 1;
      }

      return { ...state, basket };

    case ShoppingTypes.LoadProducts:
      state.products = action.payload;
      return { ...state };

    case ShoppingTypes.SetLoadingStatus:
      state.isLoading = action.payload;
      return { ...state };

    default:
      return state;
  }
}
