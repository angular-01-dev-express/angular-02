export enum ShoppingTypes {
  AddProduct = '[shooping] Add Product to basket',
  LoadProducts = '[Shopping] Load Products',
  SetLoadingStatus = '[Shopping] Set Loading Status',

  LoadProductsStore = '[Shopping] Load products to list'
}
