import { createSelector, createFeatureSelector } from '@ngrx/store';
import { IListProduct } from '../data/ilistproducts';
import { IProduct } from '../data/iproduct';

const feature = createFeatureSelector<IListProduct>('shopping');

export const allProducts = createSelector(
  feature,
  state => state.products
);

export const basket = createSelector(
  feature,
  state => state.basket
);

export const total = createSelector(
  feature,
  state => state.basket.reduce((accumulator: number, currentValue: IProduct) => accumulator + (currentValue.price * currentValue.qty), 0)
);

export const isLoading = createSelector(
  feature,
  state => state.isLoading
);
