import { ShoppingState } from './shopping.state';
import { reducer } from './shopping.reducer';

import * as ShoppingActions from './shopping.actions';
import { IProduct } from '../data/iproduct';

describe('Shopping Reducers', () => {
  const initialState: ShoppingState = {
    isLoading: false,
    products: [],
    basket: []
  };

  it('Should set loading status', () => {
    const newState = reducer(initialState, new ShoppingActions.SetLoadingStatus(true));
    expect(newState.isLoading).toBe(true);
  });

  it('Should Add a Product to basket', () => {
    const product: IProduct = {
      name: 'product 1',
      description: '',
      price: 100,
      qty: 0
    };
    const newState = reducer(initialState, new ShoppingActions.AddProduct(product));
    expect(newState.basket).toEqual([{
      name: 'product 1',
      description: '',
      qty: 1,
      price: 100
    }]);
  });

  it('Should Load products', () => {
    const products: IProduct[] = [{
      name: 'product 1',
      description: '',
      price: 100,
      qty: 0
    }, {
      name: 'product 2',
      description: '',
      price: 100,
      qty: 0
    }];
    const newState = reducer(initialState, new ShoppingActions.LoadProducts(products));
    expect(newState.products).toEqual(products);
  });
});
