import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ShoppingTypes } from './shopping.types';
import * as ShoppingActions from './shopping.actions';
import { Store } from '@ngrx/store';
import { State } from './shopping.state';
import { IProduct } from '../data/iproduct';
import { pipe } from '@angular/core/src/render3/pipe';

@Injectable()
export class ShoppingEffects {
  constructor(
    private store: Store<State>,
    private actions$: Actions
  ) {}

  @Effect() loadProductsFromDB$: Observable<any> = this.actions$.pipe(
    ofType(ShoppingTypes.LoadProductsStore),
    mergeMap((action) => {
      this.store.dispatch(new ShoppingActions.SetLoadingStatus(true));
      const products: IProduct[] = [{
        name: 'Product 1',
        description: '',
        price: 100,
        qty: 0
      }, {
        name: 'Product 2',
        description: '',
        price: 200,
        qty: 0
      }, {
        name: 'Product 3',
        description: '',
        price: 70,
        qty: 0
      }];
      const deferred = new Promise<any[]>((resolve, reject) => {
        setTimeout(() => resolve(products), 1500);
      });

      return from(deferred).pipe(
        mergeMap((productList: any) => {
          return [
            this.store.dispatch(new ShoppingActions.LoadProducts(productList)),
            this.store.dispatch(new ShoppingActions.SetLoadingStatus(false))
          ];
        })
      );
    })
  );
}
