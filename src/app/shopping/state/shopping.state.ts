import { IProduct } from '../data/iproduct';
import * as fromRoot from '../../state/app.state';

export interface ShoppingState {
  isLoading: boolean;
  products: IProduct[];
  basket: IProduct[];
}

export const initialState: ShoppingState = {
  isLoading: false,
  products: [],
  basket: []
};

export interface State extends fromRoot.State {
  products: ShoppingState;
}
